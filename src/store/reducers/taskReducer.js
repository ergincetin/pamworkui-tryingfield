import * as actionTypes from "../actions/actionTypes";

const initialState = {
  tasks: [],
  task: {},
  loading: false,
  errors: {},
};

const taskReducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.CLEAR_TASKS:
      return {...state, tasks: []};
    case actionTypes.CHANGE_TASK:
      return {...state, currentTask: action.payload};
    case actionTypes.FETCH_TASKS_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_TASKS_SUCCESS:
      return { ...state, tasks:[...state.tasks,...action.payload]};
    case actionTypes.FETCH_TASKS_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.FETCH_TASK_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_TASK_SUCCESS:
      return { ...state, task: action.payload };
    case actionTypes.FETCH_TASK_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.NEW_TASK:
      return { ...state, task: {} };
    case actionTypes.SAVE_TASK_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.SAVE_TASK_SUCCESS:
      return {...state,tasks:[],errors: {},loading: false};
    case actionTypes.SAVE_TASK_FAILED:
      return {...state,errors: action.payload.data,loading: false};
    case actionTypes.UPDATE_TASK_REQUESTED: 
      return {...state,loading: true};
    case actionTypes.UPDATE_TASK_SUCCESS: {
      return {
        ...state,tasks: [],errors: {},loading: false,
      };
    }
    case actionTypes.UPDATE_TASK_FAILED: 
      return {...state,errors: action.payload,loading: false};

    case actionTypes.DELETE_TASK_SUCCESS: {
      const id = action.payload;
      console.log(id);
      return {
        ...state,
        tasks: state.tasks.filter((item) => item.id !== id)
    }
    }
    case actionTypes.DELETE_TASK_REQUESTED:
        return {...state,loading:true};
    case actionTypes.DELETE_TASK_FAILED: 
        return {...state,errors:action.payload,loading:false};

    default:
      return state;
  }
};

export default taskReducer;
