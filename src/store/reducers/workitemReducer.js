import * as actionTypes from "../actions/actionTypes";

const initialState = {
  workitems: [],
  workitem: {},
  currentWorkitem:{},
  loading: false,
  errors: {},
};

const workitemReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CLEAR_WORKITEMS:
      return {...state, workitems:[]};
    case actionTypes.CHANGE_WORKITEM:
      return {...state, currentWorkitem: action.payload};
    case actionTypes.FETCH_WORKITEMS_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_WORKITEMS_SUCCESS:
      return { ...state, workitems: action.payload };
    case actionTypes.FETCH_WORKITEMS_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.FETCH_WORKITEM_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_WORKITEM_SUCCESS:
      return { ...state, workitem: action.payload };
    case actionTypes.FETCH_WORKITEM_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.NEW_WORKITEM:
      return { ...state, workitem: {} };
    case actionTypes.SAVE_WORKITEM_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.SAVE_WORKITEM_SUCCESS:
      return {...state,workitems: [...state.workitems, action.payload],errors: {},loading: false};
    case actionTypes.SAVE_WORKITEM_FAILED:
      return {...state,errors: action.payload.data,loading: false};
    case actionTypes.UPDATE_WORKITEM_REQUESTED: 
      return {...state,loading: true};
    case actionTypes.UPDATE_WORKITEM_SUCCESS: {
      const workitem = action.payload;
      return {
        ...state,
        workitems: state.workitems.map((item) =>
          item.id === workitem.id ? workitem : item
        ),
        errors: {},
        loading: false,
      };
    }
    case actionTypes.UPDATE_WORKITEM_FAILED: 
      return {...state,errors: action.payload,loading: false};

    case actionTypes.DELETE_WORKITEM_SUCCESS: {
      const id = action.payload;
      console.log(id);
      return {
        ...state,
        workitems: state.workitems.filter((item) => item.id !== id)
    }
    }
    case actionTypes.DELETE_WORKITEM_REQUESTED:
        return {...state,loading:true};
    case actionTypes.DELETE_WORKITEM_FAILED: 
        return {...state,errors:action.payload,loading:false};

    default:
      return state;
  }
};

export default workitemReducer;
