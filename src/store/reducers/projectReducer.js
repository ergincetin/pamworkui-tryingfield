import * as actionTypes from "../actions/actionTypes";

const initialState = {
 // projects: [],
 projects:
    {
      number:0,
      size :10,
      sort: {
          sorted:false ,
          unsorted: true,
          empty: true
      },
      totalPages: 0,
      totalElements: 0,
      content: []
  },
  project: {},
  currentProject:{},
  loading: false,
  errors: {},
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_PROJECT:
      return {...state, currentProject: action.payload};
    case actionTypes.FETCH_PROJECTS_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_PROJECTS_SUCCESS:
      return { ...state, projects: action.payload };
    case actionTypes.FETCH_PROJECTS_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.FETCH_PROJECT_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.FETCH_PROJECT_SUCCESS:
      return { ...state, project: action.payload };
    case actionTypes.FETCH_PROJECT_FAILED:
      return { ...state, loading: false, errors: action.payload };
    case actionTypes.NEW_PROJECT:
      return { ...state, project: {} };
    case actionTypes.SAVE_PROJECT_REQUESTED:
      return { ...state, loading: true };
    case actionTypes.SAVE_PROJECT_SUCCESS:
      return {...state,projects: [...state.projects.content, action.payload],errors: {},loading: false,currentProject:action.payload};
    case actionTypes.SAVE_PROJECT_FAILED:
      return {...state,errors: action.payload.data,loading: false};
    case actionTypes.UPDATE_PROJECT_REQUESTED: 
      return {...state,loading: true};
    case actionTypes.UPDATE_PROJECT_SUCCESS: {
      const project = action.payload;
      return {
        ...state,
        projects: state.projects.content.map((item) =>
          item.id === project.id ? project : item,
         
        ),
        errors: {},
        loading: false,
        currentProject:project
      };
    }
    case actionTypes.UPDATE_PROJECT_FAILED: 
      return {...state,errors: action.payload,loading: false};

    case actionTypes.DELETE_PROJECT_SUCCESS: {
      const id = action.payload;
      return {
        ...state,
        projects: state.projects.content.filter((item) => item.id !== id)
        ,currentProject:{}
    }
    }
    case actionTypes.DELETE_PROJECT_REQUESTED:
        return {...state,loading:true};
    case actionTypes.DELETE_PROJECT_FAILED: 
        return {...state,errors:action.payload,loading:false};

    default:
      return state;
  }
};

export default projectReducer;
