import { combineReducers } from "redux";
import projectReducer from "./projectReducer";
import securityReducer from "./securityReducer";
import userReducer from "./userReducer";
import workitemReducer from "./workitemReducer";
import taskReducer from "./taskReducer";
import noteReducer from "./noteReducer";
import modalReducer from "./modalReducer"

const rootReducer = combineReducers({
  userReducer,
  projectReducer,
  modalReducer,
  workitemReducer,
  taskReducer,
  noteReducer,
  security: securityReducer
});

export default rootReducer;
