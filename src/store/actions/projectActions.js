import * as actionTypes from "./actionTypes";
import axios from "../../libs/pamworkapi";


export const changeProject=(project)=>dispatch=>{
    dispatch({type:actionTypes.CHANGE_PROJECT,payload:project});
}

export const fetchProjects=(page,size)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_PROJECTS_REQUESTED});
  try{

   let url=`/projects/pagination?page=${page}&size=${size}`;
    const response = await axios.get(url);
    dispatch({type:actionTypes.FETCH_PROJECTS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_PROJECTS_FAILED});
  }
}

/*
export const fetchProjects=()=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_PROJECTS_REQUESTED});
  try{
    const response = await axios.get('/projects');
    dispatch({type:actionTypes.FETCH_PROJECTS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_PROJECTS_FAILED});
  }
}*/

export const fetchProject=(id)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_PROJECT_REQUESTED});
  try{
    const response = await axios.get(`/projects/${id}`);
    dispatch({type:actionTypes.FETCH_PROJECT_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_PROJECT_FAILED});
  }
}

export const newProject=()=>async dispatch=>
{
  dispatch({type:actionTypes.NEW_PROJECT})
}

export const saveProject=(project)=>async dispatch=>{
  dispatch({type:actionTypes.SAVE_PROJECT_REQUESTED});
  try{
    const response = await axios.post(`/projects`,project);
    dispatch({type:actionTypes.SAVE_PROJECT_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.SAVE_PROJECT_FAILED});
  }
}


export const updateProject=(project)=>async dispatch=>{
  dispatch({type:actionTypes.UPDATE_PROJECT_REQUESTED});
  try{
    await axios.put(`/projects/${project.id}`,project);
    dispatch({type:actionTypes.UPDATE_PROJECT_SUCCESS,payload:project});
  }
  catch
  {
    dispatch({type:actionTypes.UPDATE_PROJECT_FAILED});
  }
}

export const deleteProject=(id)=>async dispatch=>{
  dispatch({type:actionTypes.DELETE_PROJECT_REQUESTED});
  try{
    await axios.delete(`/projects/${id}`);
    dispatch({type:actionTypes.DELETE_PROJECT_SUCCESS,payload:id});
  }
  catch
  {
    dispatch({type:actionTypes.DELETE_PROJECT_FAILED});
  }
}
