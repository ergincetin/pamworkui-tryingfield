import * as actionTypes from "./actionTypes";
import axios from "../../libs/pamworkapi";


export const changeTask=(task)=>dispatch=>{
  dispatch({type:actionTypes.CHANGE_TASK,payload:task});
}

export const clearTasks=()=>dispatch=>{
  dispatch({type:actionTypes.CLEAR_TASKS,payload:[]});
}

export const fetchTasks=(workitem)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_TASKS_REQUESTED});
  try{

    let url='/tasks';

    if(workitem!==null)
    {
       url=`/tasks?workitemid=${workitem.id}`;
    }

    const response = await axios.get(url);
    dispatch({type:actionTypes.FETCH_TASKS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_TASKS_FAILED});
  }
}

export const fetchTasksByUsername=(username)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_TASKS_REQUESTED});
  try{

    let url='/tasks';

    if(username!==null)
    {
       url=`/tasks?username=${username}`;
    }

    const response = await axios.get(url);
    dispatch({type:actionTypes.FETCH_TASKS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_TASKS_FAILED});
  }
}



export const fetchTask=(id)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_TASK_REQUESTED});
  try{
    const response = await axios.get(`/tasks/${id}`);
    dispatch({type:actionTypes.FETCH_TASK_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_TASK_FAILED});
  }
}

export const newTask=()=>async dispatch=>
{
  dispatch({type:actionTypes.NEW_TASK})
}

export const saveTask=(task)=>async dispatch=>{
  dispatch({type:actionTypes.SAVE_TASK_REQUESTED});
  try{
  
     const response = await axios.post(`/tasks`,task);
    dispatch({type:actionTypes.SAVE_TASK_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.SAVE_TASK_FAILED});
  }
}


export const updateTask=(task)=>async dispatch=>{
  dispatch({type:actionTypes.UPDATE_TASK_REQUESTED});
  try{
    await axios.put(`/tasks/${task.id}`,task);
    dispatch({type:actionTypes.UPDATE_TASK_SUCCESS,payload:task});
  }
  catch
  {
    dispatch({type:actionTypes.UPDATE_TASK_FAILED});
  }
}

export const deleteTask=(id)=>async dispatch=>{
  dispatch({type:actionTypes.DELETE_TASK_REQUESTED});
  try{
    await axios.delete(`/tasks/${id}`);
    dispatch({type:actionTypes.DELETE_TASK_SUCCESS,payload:id});
  }
  catch
  {
    dispatch({type:actionTypes.DELETE_TASK_FAILED});
  }
}
