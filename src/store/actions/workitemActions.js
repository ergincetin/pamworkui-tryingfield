import * as actionTypes from "./actionTypes";
import axios from "../../libs/pamworkapi";


export const changeWorkitem=(workitem)=>dispatch=>{
  dispatch({type:actionTypes.CHANGE_WORKITEM,payload:workitem});
}

export const clearWorkitems=()=>dispatch=>{
  dispatch({type:actionTypes.CLEAR_WORKITEMS,payload:[]});
}

export const fetchWorkitems=(project)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_WORKITEMS_REQUESTED});
  try{

    let url='/workitems';

    if(project!==null)
    {
       url=`/workitems?projectid=${project.id}`;
    }

    const response = await axios.get(url);
    dispatch({type:actionTypes.FETCH_WORKITEMS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_WORKITEMS_FAILED});
  }
}

export const fetchWorkitemsByUsername=(username)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_WORKITEMS_REQUESTED});
  try{

    let url='/workitems';

    if(username!==null)
    {
       url=`/workitems?username=${username}`;
    }

    const response = await axios.get(url);
    dispatch({type:actionTypes.FETCH_WORKITEMS_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_WORKITEMS_FAILED});
  }
}



export const fetchWorkitem=(id)=>async dispatch=>{
  dispatch({type:actionTypes.FETCH_WORKITEM_REQUESTED});
  try{
    const response = await axios.get(`/workitems/${id}`);
    dispatch({type:actionTypes.FETCH_WORKITEM_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.FETCH_WORKITEM_FAILED});
  }
}

export const newWorkitem=()=>async dispatch=>
{
  dispatch({type:actionTypes.NEW_WORKITEM})
}

export const saveWorkitem=(project)=>async dispatch=>{
  dispatch({type:actionTypes.SAVE_WORKITEM_REQUESTED});
  try{
  
     const response = await axios.post(`/workitems`,project);
    dispatch({type:actionTypes.SAVE_WORKITEM_SUCCESS,payload:response.data});
  }
  catch
  {
    dispatch({type:actionTypes.SAVE_WORKITEM_FAILED});
  }
}


export const updateWorkitem=(project)=>async dispatch=>{
  dispatch({type:actionTypes.UPDATE_WORKITEM_REQUESTED});
  try{
    await axios.put(`/workitems/${project.id}`,project);
    dispatch({type:actionTypes.UPDATE_WORKITEM_SUCCESS,payload:project});
  }
  catch
  {
    dispatch({type:actionTypes.UPDATE_WORKITEM_FAILED});
  }
}

export const deleteWorkitem=(id)=>async dispatch=>{
  dispatch({type:actionTypes.DELETE_WORKITEM_REQUESTED});
  try{
    await axios.delete(`/workitems/${id}`);
    dispatch({type:actionTypes.DELETE_WORKITEM_SUCCESS,payload:id});
  }
  catch
  {
    dispatch({type:actionTypes.DELETE_WORKITEM_FAILED});
  }
}
