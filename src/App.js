import { PureComponent } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import "./App.css";
import { Layout } from "antd";
import NavMenu from "./components/_Layout/Navi/NavMenu";

import jwt_decode from "jwt-decode";
import setJWTToken from "./libs/SetJWTToken";
import { SET_CURRENT_USER } from "./store/actions/actionTypes";
import { logout } from "./store/actions/securityActions";
import configureStore from "./store/reducers/configureStore";
import SecuredRoute from "./libs/SecuredRoute";
import Signup from "./components/Auth/Signup";
import Signin from "./components/Auth/Signin";
import Signout from "./components/Auth/Signout";
import Home from "./components/Home";
import ProjectDetails from "./components/Projects/ProjectDetails";
import ProjectList from "./components/Projects/ProjectList";
import ProjectNew from "./components/Projects/ProjectNew";
import ProjectEdit from "./components/Projects/ProjectEdit";
import ProjectDelete from "./components/Projects/ProjectDelete";
import NoteDetails from "./components/Notes/NoteDetails"
import NoteList from "./components/Notes/NoteList";
import NoteNew from "./components/Notes/NoteNew";
import NoteEdit from "./components/Notes/NoteEdit";
import NoteDelete from "./components/Notes/NoteDelete";
import WorkitemList from "./components/Workitems/WorkitemList";
import WorkitemNew from "./components/Workitems/WorkitemNew";
import WorkitemEdit from "./components/Workitems/WorkitemEdit";
import WorkitemDetails from "./components/Workitems/WorkitemNew";
import WorkitemDelete from "./components/Workitems/WorkitemDelete";
import TaskList from "./components/Tasks/TaskList";
import TaskNew from "./components/Tasks/TaskNew";
import TaskEdit from "./components/Tasks/TaskEdit";
import TaskDetails from "./components/Tasks/TaskDetails";
import TaskDelete from "./components/Tasks/TaskDelete";

const jwtToken = localStorage.getItem("token");
console.log(jwtToken);


const store = configureStore();
if (jwtToken) {
  setJWTToken(jwtToken);
  const decoded_jwtToken = jwt_decode(jwtToken);
  store.dispatch({
    type: SET_CURRENT_USER,
    payload: decoded_jwtToken,
  });
// eslint-disable-next-line no-restricted-globals
  const currentTime = Date.now() / 1000;
 
  if (decoded_jwtToken.exp < currentTime) {
    store.dispatch(logout());

    /* eslint no-restricted-globals:0 */
    window.location = "/";
  }
}

class App extends PureComponent {
  render() {
    const { Header, Content } = Layout;
    return (
      <Provider store={store}>
        <Router history={history}>
          <div>
            <Layout>
              <Header>
                <NavMenu />
              </Header>
              <Content>
              <Route exact path="/" component={Home} />
                <Route exact path="/signin" component={Signin} />
                <Route exact path="/signup" component={Signup} />
                <Switch> 
                  <SecuredRoute exact path="/signout" component={Signout} />
                  <SecuredRoute exact path="/projects-list" component={ProjectList} />
                  <SecuredRoute exact path="/projects-detail/:id" component={ProjectDetails} />
                  <SecuredRoute exact path="/projects-new" component={ProjectNew} />
                  <SecuredRoute exact path="/projects-edit/:id" component={ProjectEdit} />
                  <SecuredRoute exact path="/projects-delete/:id" component={ProjectDelete} />
                  <SecuredRoute exact path="/notes-list" component={NoteList}/>
                  <SecuredRoute exact path="/notes-detail/:id" component={NoteDetails}/>
                  <SecuredRoute exact path="/notes-new" component={NoteNew}/>
                  <SecuredRoute exact path="/notes-edit/:id" component={NoteEdit}/>
                  <SecuredRoute exact path="/notes-delete/:id" component={NoteDelete}/>
                  <SecuredRoute exact path="/workitems-list/" component={WorkitemList}/>
                  <SecuredRoute exact path="/workitems-new/" component={WorkitemNew}/>
                  <SecuredRoute exact path="/workitems-edit/:id" component={WorkitemEdit}/>
                  <SecuredRoute exact path="/workitems-detail/:id" component={WorkitemDetails}/>
                  <SecuredRoute exact path="/workitems-delete/:id" component={WorkitemDelete}/>
                  <SecuredRoute exact path="/tasks-list/" component={TaskList}/>
                  <SecuredRoute exact path="/tasks-new/" component={TaskNew}/>
                  <SecuredRoute exact path="/tasks-edit/:id" component={TaskEdit}/>
                  <SecuredRoute exact path="/tasks-detail/:id" component={TaskDetails}/>
                  <SecuredRoute exact path="/tasks-delete/:id" component={TaskDelete}/>
                </Switch>
              </Content>
            </Layout>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
