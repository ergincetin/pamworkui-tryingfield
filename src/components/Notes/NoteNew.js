import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import NoteForm from "./NoteForm";
import { newNote, saveNote } from "../../store/actions/noteActions";


class NoteNew extends PureComponent {
  
  constructor(props) {
    super();
    this.state = {
      redirect: false,
      initialValues:{
        name: "",
        description: ""
      },
    };
  }

  componentDidMount = async () => {
    await this.props.newNote();
  };
  handleSubmit = (formProps) => {
    const {
      name,
      description
    } = formProps;

    return this.props
      .saveNote(formProps)
      .then((response) => this.setState({ redirect: true }));
  };

  render() {

    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/notes-list" />
        ) : (
          <Formik
            initialValues={this.state.initialValues}
            enableReinitialize={true}
            onSubmit={this.handleSubmit}
          >
            {NoteForm}
          </Formik>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  note: state.noteReducer.note,
  errors: state.noteReducer.errors,
  currentNote: state.noteReducer.currentNote,

});

const mapDispatchToProps = {
  newNote,
  saveNote
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteNew);
