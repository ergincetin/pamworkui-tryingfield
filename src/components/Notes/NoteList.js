import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Button, Avatar ,Pagination, Input} from "antd";
import SearchBar from "../_Layout/SearchBar/SearchBar"

import {
  changeNote,
  fetchNotes,
  deleteNote,
} from "../../store/actions/noteActions";

import { Link } from "react-router-dom";

const {Search} = Input;
class NoteList extends Component {
  componentDidMount() {
    this.props.fetchNotes(0,10);
  }
  selectNote = (note) => {
    this.props.changeNote(note);
  };
  handleChange = value => {
    if(value)
      this.props.fetchNotes(value-1,10);
  };
  render() {
   const notes= this.props.notes;
    return (
      
      <div className="site-layout-content">
        <div>
          <SearchBar/>
       

        </div>
        <div>
          <Button type="link" href={`/notes-new`}>
            Add Note
          </Button>
        </div>
        <List
          itemLayout="horizontal"
          dataSource={notes.content}
          renderItem={(item) => (
            <List.Item>
              <List.Item.Meta
                avatar={
                  <Avatar src="https://www.flaticon.com/svg/static/icons/svg/2579/2579987.svg" />
                }
                title={item.id}
                description={
                  <>
                    {item.name}
                    <Link
                      onClick={() => this.selectNote(item)}
                      to={{
                        pathname: `notes-detail/${item.id}`,
                        state: { item },
                      }}
                    >
                      DETAIL
                    </Link>
                  </>
                }
              />
            </List.Item>
          )}
        />
        <Pagination defaultCurrent={1} total={notes.totalElements} onChange={this.handleChange}/>
      </div>
     /* <div>
      {JSON.stringify(notes)}
      </div>*/
    );
  }
}

const mapStateToProps = (state) => ({
  notes: state.noteReducer.notes,
  currentNote: state.noteReducer.currentNote,
});

const mapDispatchToProps = {
  changeNote,
  fetchNotes,
  deleteNote,
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteList);
