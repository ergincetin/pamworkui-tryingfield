import React from "react";
import { Form, Input, Row, Col} from "antd";
// import { Form, Field } from "formik";
import { AntInput } from "../../libs/CreateAndFields";
import ReactQuillFormik from "../../libs/ReactQuillFormik";

const NoteFormAntd = () => {
    return (
    <Form>
        <Row>
        <Col span={24}>
          <Form.Item
           name="name"
           type="text"
           label="name"
           placeholder="Name"
           hasFeedback>
               <AntInput/>
          </Form.Item>
        </Col>
      </Row>
     
      <Row>
       <p>Description</p>
      </Row>
      <Row>
         <Col span={24}>
            <Form.Item
            name="description"
            type="text"
            label="Description"
            
          >
              <Input/>
            {({ field }) => ReactQuillFormik(field)}
            </Form.Item>   
        </Col> 
      </Row>
      <Row>
     <div className="submit-container">
          <button className="ant-btn ant-btn-primary" type="submit">
            Submit
          </button>
        </div> 
      </Row>
    </Form>)
}

export default NoteFormAntd;
