import React, {useState} from "react";
import {Modal, Button, Card} from "antd"
import NoteForm from "./NoteForm"
import ProjectForm from "../Projects/ProjectForm"
import NoteBasic from "./NoteFormBasic";
import NoteFormAntd from "./NoteFormAntd";
// import {closeModal} from "../Modals/ModalActions"

const NoteEditModal = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);
  
    const showModal = () => {
      setIsModalVisible(true);
    };
  
    const handleOk = () => {
      setIsModalVisible(false);
    };
  
    const handleCancel = () => {
      setIsModalVisible(false);
    };
  

  
    return (
      <>
        <Button type="primary" onClick={showModal}>
          Edit
        </Button>
        <Modal 
          title="Basic Modal"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          okText="Save Changes"
          cancelText="Close"
        >
            <NoteFormAntd/>
          {/* <Button onClick={()=>showContent}>Button sontent</Button>
          <p>Empty contents</p>
          <p>Some contents</p> */}


        </Modal>
      </>
    );
  };

  export default NoteEditModal;