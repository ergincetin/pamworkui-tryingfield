import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Card, Button } from "antd";

import {
  changeNote,
  deleteNote,
} from "../../store/actions/noteActions";

import { Link } from "react-router-dom";
const style = { padding: "8px" };
class NoteDelete extends Component {
  state = {
    redirect: false,
  };
  removeNote = (id) => {
    this.props
      .deleteNote(id)
      .then((response) => this.setState({ redirect: true }));
  };
  render() {
    const note = this.props.currentNote;
    if (!this.props.currentNote) {
      return <div>Loading...</div>;
    }

    return (
      <div className="site-layout-content">
        <>
          {this.state.redirect ? (
            <Redirect to="/notes-list" />
          ) : (
            <Card size="small" title="Note Delete" style={{ width: 300 }}>
              <p>{note.id} . kaydını silmek istiyor musunuz?</p>
              <Link
                as="Button"
                to={{
                  pathname: `/notes-details/${note.id}`,
                }}
              >
                Hayır
              </Link>
              <Button
                type="danger"
                onClick={() => this.removeNote(note.id)}
              >
                Evet
              </Button>
            </Card>
          )}
        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentNote: state.noteReducer.currentNote,
});

const mapDispatchToProps = {
  changeNote,
  deleteNote,
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteDelete);
