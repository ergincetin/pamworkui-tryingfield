import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import NoteForm from "./NoteForm";

import { updateNote } from "../../store/actions/noteActions";


class NoteEdit extends Component {
  constructor(props) {
    super();
    this.state = {
      redirect: false,
    };
  }
  submit = (note) => {
    return this.props
      .updateNote(note)
      .then((response) => this.setState({ redirect: true }))
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    const note = this.props.currentNote;
    if (!this.props.currentNote) {
      return <div>Loading...</div>;
    }
    const formValues = {
      id: note.id,
      name: note.name,
      description: note.description,
    };
    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/notes-list" />
        ) : (
          <Formik initialValues={formValues} onSubmit={this.submit}>
            {NoteForm}
          </Formik>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentNote: state.noteReducer.currentNote,
});

const mapDispatchToProps = {
  updateNote
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteEdit);
