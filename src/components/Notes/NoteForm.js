import React from "react";
import {  Row, Col} from "antd";
import { Form, Field } from "formik";
import { AntDatePicker, AntInput, AntSelect } from "../../libs/CreateAndFields";
import { isRequired } from "../../libs/ValidateFields";
import { dateFormat } from "../../libs/FieldFormats";
import moment from "moment";

import ReactQuillFormik from "../../libs/ReactQuillFormik";

const NoteForm = ({ handleSubmit, values, submitCount }) => {
  return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <Row>
        <Col span={24}>
          <Field
            component={AntInput}
            name="name"
            type="text"
            label="Name"
            validate={isRequired}
            submitCount={submitCount}
            hasFeedback
          />
        </Col>
      </Row>
     
      <Row>
       <p>Description</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="description"
            type="text"
            label="Description"
            submitCount={submitCount}
          >
              {({ field }) => ReactQuillFormik(field)}
            </Field>
        </Col>
      </Row>
      
      
      <Row>
        <div className="submit-container">
          <button className="ant-btn ant-btn-primary" type="submit">
            Submit
          </button>
        </div>
      </Row>
    </Form>
  );
};

export default NoteForm;
