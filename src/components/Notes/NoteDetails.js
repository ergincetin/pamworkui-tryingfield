import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Descriptions, Row, Col } from "antd";
import {
  changeNote,
  deleteNote,
} from "../../store/actions/noteActions";
import moment from "moment";
import { dateFormat, timeFormat } from "../../libs/FieldFormats";
import { Link } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";
import NoteEditModal from "./NoteEditModal"
const style = { padding: "8px" };

class NoteDetails extends Component {
  selectNote = (note) => {
    this.props.changeNote(note);
  };
  render() {
    const note = this.props.currentNote;
    if (!this.props.currentNote) {
      return <div>Loading...</div>;
    }

    return (
      <div className="site-layout-content">
        <Descriptions
          bordered
          title="Note Detail"
          extra={
            <div>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    {/* <Link
                      onClick={() => this.selectNote(note)}
                      to={{
                        pathname: `/notes-edit/${note.id}`,
                        state: { note },
                      }}
                    >
                      Edit
                    </Link>  */}
                    <NoteEditModal/>
                   {/*  find solution to render the compoeneti in scope of modal */}
                  </div>
                </Col>
                <Col span={1}/>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectNote(note)}
                      to={{
                        pathname: `/notes-delete/${note.id}`,
                        state: { note },
                      }}
                    >
                      Delete
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
          }
        >
          <Descriptions.Item label="Id">{note.id}</Descriptions.Item>
          <Descriptions.Item label="Name">{note.name}</Descriptions.Item>
          <Descriptions.Item label="Description">
            {ReactHtmlParser(note.description)}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentNote: state.noteReducer.currentNote,
});

const mapDispatchToProps = {
  changeNote,
  deleteNote,
};

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetails);
