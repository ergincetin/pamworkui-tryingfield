import React from "react"
import {Form, Input, Button, Layout, Row} from "antd"
import {AntInput} from "../../libs/CreateAndFields"

const NoteBasic =()=> {
    return (
      <Layout style={{backgroundColor: "white"}}>
      <Form>
        <Form.Item
        label="Username"
        name="username"
      >
        <AntInput />
      </Form.Item>
      <Form.Item
      label="Lastname">
        <AntInput/>
      </Form.Item>
      </Form>
</Layout>
    )
}
export default NoteBasic;