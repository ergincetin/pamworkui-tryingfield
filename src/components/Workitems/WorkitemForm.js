import React from "react";
import { Row, Col } from "antd";
import { Form, Field } from "formik";
import { AntDatePicker, AntInput, AntSelect } from "../../libs/CreateAndFields";
import { isRequired } from "../../libs/ValidateFields";
import { dateFormat } from "../../libs/FieldFormats";
import moment from "moment";

import ReactQuillFormik from "../../libs/ReactQuillFormik";

const WorkitemForm = ({ handleSubmit, values, submitCount }) => {
  return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <Row>
        <Col span={24}>
          <Field
            component={AntInput}
            name="name"
            type="text"
            label="Name"
            validate={isRequired}
            submitCount={submitCount}
            hasFeedback
          />
        </Col>
      </Row>
      <Row>
        <Col span={6}>
          <Field
            component={AntInput}
            name="point"
            type="text"
            label="Point"
            submitCount={submitCount}
          />
        </Col>
        <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="state"
            defaultValue={values.state}
            selectOptions={values.selectOptionsState}
            label="State"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="category"
            defaultValue={values.category}
            selectOptions={values.selectOptionsCategory}
            label="Category"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="type"
            defaultValue={values.type}
            selectOptions={values.selectOptionsType}
            label="Type"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
      </Row>
      <Row>
      <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="responsibleUser"
            defaultValue={values.responsibleUser}
            selectOptions={values.selectoptionsRU}
            label="Responsible User"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={6}>
          <Field
            component={AntDatePicker}
            name="dueDate"
            label="Due Date"
            value={values.dueDate ? moment(values.dueDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
        <Col span={6}>
          <Field
            component={AntDatePicker}
            name="expectedDate"
            label="Expected Date"
            value={values.expectedDate ? moment(values.expectedDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
        <Col span={6}>
          <Field
            component={AntDatePicker}
            name="actualDate"
            label="Actual Date"
            value={values.actualDate ? moment(values.actualDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
      </Row>
      <Row>
        <p>Description</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="description"
            type="text"
            label="Description"
            submitCount={submitCount}
          >
            {({ field }) => ReactQuillFormik(field)}
          </Field>
        </Col>
      </Row>
    
      <Row>
        <div className="submit-container">
          <button className="ant-btn ant-btn-primary" type="submit">
            Submit
          </button>
        </div>
      </Row>
    </Form>
  );
};

export default WorkitemForm;
