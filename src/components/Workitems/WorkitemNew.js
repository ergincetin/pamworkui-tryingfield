import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import WorkitemForm from "./WorkitemForm";
import { newWorkitem, saveWorkitem } from "../../store/actions/workitemActions";

import { fetchUsers } from "../../store/actions/userActions";

class WorkitemNew extends PureComponent {
  
  constructor(props) {
    super();
    this.state = {
      redirect: false,
      initialValues:{
        name: "",
        description: "",
        point: "",
        dueDate: "",
        expectedDate: "",
        actualDate: "",
        responsibleUser:"",
        state:"",
        type:"",
        category:"",
        projectid:"",
        selectOptionsState: [
          "Cancelled",
          "Blocked",
          "InBackLog",
          "WIP",
          "ReadyToTest",
          "InTest",
          "Deployed",
          "Released"
        ],
        selectOptionsCategory: [
            "Analysis",
            "Development",
            "Test",
            "Operation",
            "Document"
          ],
          selectOptionsType: [
            "Request",
            "UserStory",
            "Bug",
            "Ticket"
          ],
      },
    };
  }
// State:   Cancelled(-1),Blocked(0),InBackLog(1),WIP(2),ReadyToTest(3),InTest(4),Deployed(5),Released(6);
// Category: Analysis(1),Development(2),Test(3),Operation(4),Document(5);
// Type:   Request(1),UserStory ( 2),Bug ( 3),Ticket(4);
  componentDidMount = async () => {
    await this.props.newWorkitem();
    await this.props.fetchUsers();
    const selectoptionsRU = this.props.usernames;
    const currentProject= this.props.currentProject;
    this.setState({
      initialValues:{ ...this.state.initialValues, selectoptionsRU,projectid:currentProject.id},
    });
  };
  handleSubmit = (formProps) => {
    const {
      name,
      description,
      point,
      dueDate,
      expectedDate,
      actualDate,
      responsibleUser,
      type,
      category,
      state,
      projectid,
    } = formProps;

    return this.props
      .saveWorkitem(formProps)
      .then((response) => this.setState({ redirect: true }));
  };

  render() {

    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik
            initialValues={this.state.initialValues}
            enableReinitialize={true}
            onSubmit={this.handleSubmit}
          >
            {WorkitemForm}
          </Formik>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  workitem: state.workitemReducer.workitem,
  errors: state.projectReducer.errors,
  currentProject: state.projectReducer.currentProject,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map((user) => user["username"]),
});

const mapDispatchToProps = {
  newWorkitem,
  saveWorkitem,
  fetchUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkitemNew);
