import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchWorkitems,
  deleteWorkitem,
  changeWorkitem,
} from "../../store/actions/workitemActions";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { fetchTasks,clearTasks } from "../../store/actions/taskActions";
import { Button,Row, Col,Collapse ,List} from "antd";
import ReactHtmlParser from "react-html-parser";
import TaskList from "../Tasks/TaskList";

const { Panel } = Collapse;

class WorkitemList extends Component {
  removeWorkitem = (id) => {
    this.props.deleteWorkitem(id);
  };
  selectWorkitem = (workitem) => {
    this.props.changeWorkitem(workitem);
    this.props.fetchTasks(workitem);
  };
  /* addTask = (workitem) => {
    this.props.changeWorkitem(workitem);
  this.props.clearTasks();
    this.props.history.push("/tasks-new");
  }; */

  render() {
    return (
      <div>
        <br></br>
        <br></br>
        <Row>
          <Col sm={7}>
            <h3>Workitem List</h3>
          </Col>
          <Col></Col>
          <Col sm={2}>
           {/*  <Button as={Link} to="/workitems-new" variant="outline-primary">
              New Workitem
            </Button> */}
          </Col>
        </Row>
        <Collapse accordion>
          {this.props.workitems.map((workitem) => (
            <Panel header={workitem.name} key={workitem.id}>
              <Row>
                <Col>
                <Link
                      onClick={() => this.selectWorkitem(workitem)}
                      to={{
                        pathname: `workitems-edit/${workitem.id}`,
                        state: { workitem },
                      }}
                    >
                      Edit Workitem
                    </Link>
                    <Link
                      onClick={() => this.selectWorkitem(workitem)}
                      to={{
                        pathname: `tasks-list`,
                        state: { workitem },
                      }}
                    >
                      Task List
                    </Link>
                    <Link
                      onClick={() => this.selectWorkitem(workitem)}
                      to={{
                        pathname: `tasks-new`,
                        state: { workitem },
                      }}
                    >
                      New Task
                    </Link>
                </Col>
              </Row>
              <Row>
                <Col>Hours</Col>
                <Col>Due Date</Col>
                <Col>Expected Date</Col>
                <Col>Actual Date</Col>
                <Col>Responsible User</Col>
                <Col>State</Col>
                <Col>Category </Col>
                <Col>Type</Col>
              </Row>
              <Row>
                <Col>{workitem.point}</Col>
                <Col>
                  {workitem.dueDate === undefined
                    ? new Date(workitem.dueDate).toLocaleDateString()
                    : workitem.dueDate}
                </Col>
                <Col>
                  {workitem.expectedDate === undefined
                    ? new Date(workitem.expectedDate).toLocaleDateString()
                    : workitem.expectedDate}
                </Col>
                <Col>
                  {workitem.actualDate === undefined
                    ? new Date(workitem.actualDate).toLocaleDateString()
                    : workitem.actualDate}
                </Col>
                <Col>{workitem.responsibleUser}</Col>
                <Col>{workitem.state}</Col>
                <Col>{workitem.category}</Col>
                <Col>{workitem.type}</Col>
              </Row>
              <p>{ReactHtmlParser(workitem.description)}</p>
              <TaskList workitem={workitem}/>
            </Panel>
          ))}
           

        </Collapse>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  workitems: state.workitemReducer.workitems,
  currentProject: state.projectReducer.changeProject,
  tasks: state.taskReducer.tasks,
});

const mapDispatchToProps = {
  fetchWorkitems,
  deleteWorkitem,
  changeWorkitem,
  fetchTasks,
  clearTasks,
};

const WorkitemListWithRouter = withRouter(WorkitemList);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WorkitemListWithRouter);
