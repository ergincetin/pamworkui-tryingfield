import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import WorkitemForm from "./WorkitemForm";

import { updateWorkitem } from "../../store/actions/workitemActions";
import { fetchUsers } from "../../store/actions/userActions";

class WorkitemEdit extends Component {
  constructor(props) {
    super();
    this.state = {
      redirect: false,
    };
  }
  componentDidMount=async()=> {

   await this.props.fetchUsers();
  }
 

  submit = (workitem) => {
    return this.props
      .updateWorkitem(workitem)
      .then((response) => this.setState({ redirect: true }))
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    const workitem = this.props.currentWorkitem;
    if (!this.props.currentWorkitem) {
      return <div>Loading...</div>;
    }
    const formValues = {
        id:workitem.id,
        name:workitem.name,
        description:workitem.description,
        point:workitem.point,
        dueDate:workitem.dueDate,
        expectedDate:workitem.expectedDate,
        actualDate:workitem.actualDate,
        responsibleUser: workitem.responsibleUser,
        type:workitem.type,
        category:workitem.category,
        state: workitem.state,
        projectid:workitem.projectid,
        selectoptionsRU: this.props.usernames,
        selectOptionsState: [
            "Cancelled",
            "Blocked",
            "InBackLog",
            "WIP",
            "ReadyToTest",
            "InTest",
            "Deployed",
            "Released"
          ],
          selectOptionsCategory: [
              "Analysis",
              "Development",
              "Test",
              "Operation",
              "Document"
            ],
            selectOptionsType: [
              "Request",
              "UserStory",
              "Bug",
              "Ticket"
            ]
    };
    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik initialValues={formValues} onSubmit={this.submit}>
            {WorkitemForm}
          </Formik>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentWorkitem: state.workitemReducer.currentWorkitem,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map(user => user["username"])
});

const mapDispatchToProps = {
  updateWorkitem,
  fetchUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkitemEdit);
