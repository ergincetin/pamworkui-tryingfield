import React, {PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Card, Button } from "antd";
import {
  changeWorkitem,
  deleteWorkitem
} from "../../store/actions/workitemActions";
import { Link } from "react-router-dom";

class WorkitemDelete extends PureComponent {
state = {
            redirect: false,
          };
          removeWorkitem = (id) => {
            this.props
              .deleteWorkitem(id)
              .then((response) => this.setState({ redirect: true }));
          };

    render() {
        const workitem = this.props.currentWorkitem;
        if (!this.props.currentProject) {
          return <div>Loading...</div>;
        }
        
        return (
            <div className="site-layout-content">
        <>
          {this.state.redirect ? (
            <Redirect to="/workitems-list" />
          ) : (
            <Card size="small" title="Workitem Delete" style={{ width: 300 }}>
              <p>{workitem.id} . kaydını silmek istiyor musunuz?</p>
              <Link
                as="Button"
                to={{
                  pathname: `/workitems-details/${workitem.id}`,
                }}
              >
                No
              </Link>
              <Button
                type="danger"
                onClick={() => this.removeWorkitem(workitem.id)}
              >
               Yes
              </Button>
            </Card>
          )}
        </>
      </div>
        )
    }
}

const mapStateToProps = (state) => ({
    currentWorkitem: state.workitemReducer.currentWorkitem,
});


const mapDispatchToProps = {
    changeWorkitem, 
    deleteWorkitem
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkitemDelete);