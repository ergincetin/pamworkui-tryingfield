import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Descriptions, Row, Col } from "antd";
import {
  changeWorkitem,
  deleteWorkitem,
} from "../../store/actions/workitemActions";
import moment from "moment";
import { dateFormat, timeFormat } from "../../libs/FieldFormats";
import { Link } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";

const style = { padding: "8px" };

class WorkitemDetails extends PureComponent {
    selectWorkitem = (workitem) => {
        this.props.changeWorkitem(workitem);
    }

    render() {
        const workitem = this.props.currentWorkitem;
        if(!this.props.currentWorkitem){
            return <div>Loading...</div>;
        }
        return (
            <div className="site-layout-content">
               <Descriptions
          bordered
          title="Workitem Detail"
          extra={
            <div>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectWorkitem(workitem)}
                      to={{
                        pathname: `/workitems-edit/${workitem.id}`,
                        state: { workitem },
                      }}
                    >
                      Edit
                    </Link>
                  </div>
                </Col>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectWorkitem(workitem)}
                      to={{
                        pathname: `/workitem-delete/${workitem.id}`,
                        state: { workitem },
                      }}
                    >
                      Delete
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
          }
        >
          <Descriptions.Item label="Id">{workitem.id}</Descriptions.Item>
          <Descriptions.Item label="Name">{workitem.name}</Descriptions.Item>
          <Descriptions.Item label="Workitem Point">
            {workitem.point}
          </Descriptions.Item>
          <Descriptions.Item label="Workitem State">
            {workitem.projectMaster}
          </Descriptions.Item>

          <Descriptions.Item label="Workitem Responsible User">
            {ReactHtmlParser(workitem.responsibleUser)}
          </Descriptions.Item>

          <Descriptions.Item label="Workitem Due Date">
            {workitem.startDate
              ? moment(workitem.dueDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
          <Descriptions.Item label="Workitem Expected Date">
            {workitem.expectedDate
              ? moment(workitem.expectedDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
            
          <Descriptions.Item label="Actual Date">
            {workitem.actualDate
              ? moment(workitem.actualDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
          <Descriptions.Item label="Workitem Type">
            {ReactHtmlParser(workitem.type)}
          </Descriptions.Item>
          <Descriptions.Item label="Workitem Category">
            {ReactHtmlParser(workitem.category)}
          </Descriptions.Item>
        </Descriptions>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    currentWorkitem: state.workitemReducer.currentWorkitem,
});

const mapDispatchToProps = {
    changeWorkitem,
    deleteWorkitem,
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkitemDetails);