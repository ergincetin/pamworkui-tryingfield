import React, { PureComponent } from "react";
import { Button, Layout, Modal } from "antd";
import { Link } from "react-router-dom";
import {
  changeProject,
  deleteProject,
} from "../../store/actions/projectActions";
import { openModal, closeModal } from "../../store/actions/modalActions";
import {connect} from "react-redux"

class EditModal extends PureComponent {
  selectProject = (project) => {
    this.props.changeProject(project);
  };
  render() {
    const project = this.props.currentProject;
    if (!this.props.currentProject) {
      return <div>Loading...</div>;
    }

    return (
        <div>
        <Button onClick={()=>openModal('MODAL_OPEN', {})}></Button>
        <Modal 
        onCancel={()=>closeModal}
        title="Edit Project" centered>
        <Link
          onClick={() => this.selectProject(project)}
          to={{
            pathname: `/projects-edit/${project.id}`,
            state: { project },
          }}
        >
          Edit
        </Link>
      </Modal>
      </div>
    );
  }
}

const mapStateToProps =(state) => ({
    currentProject: state.projectReducer.currentProject,
})

const mapDispatchToProps = {
    deleteProject,
    changeProject, 
    openModal,
    closeModal
}

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);