import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Descriptions, Row, Col } from "antd";
import {
  changeProject,
  deleteProject,
} from "../../store/actions/projectActions";
import moment from "moment";
import { dateFormat, timeFormat } from "../../libs/FieldFormats";
import { Link } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";
const style = { padding: "8px" };

class ProjectDetails extends Component {
  selectProject = (project) => {
    this.props.changeProject(project);
  };
  render() {
    const project = this.props.currentProject;
    if (!this.props.currentProject) {
      return <div>Loading...</div>;
    }
    return (
      <div className="site-layout-content">
        <Descriptions
          bordered
          title="Project Detail"
          extra={
            <div>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectProject(project)}
                      to={{
                        pathname: `/projects-edit/${project.id}`,
                        state: { project },
                      }}
                    >
                      Edit
                    </Link>
                  </div>
                </Col>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectProject(project)}
                      to={{
                        pathname: `/projects-delete/${project.id}`,
                        state: { project },
                      }}
                    >
                      Delete
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
          }
        >
          <Descriptions.Item label="Id">{project.id}</Descriptions.Item>
          <Descriptions.Item label="Name">{project.name}</Descriptions.Item>
          <Descriptions.Item label="Project Status">
            {project.status}
          </Descriptions.Item>
          <Descriptions.Item label="Project Manager">
            {project.projectMaster}
          </Descriptions.Item>

          <Descriptions.Item label="Project Start Date">
            {project.startDate
              ? moment(project.startDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
          <Descriptions.Item label="Project Finish Date">
            {project.finishDate
              ? moment(project.finishDate).format(dateFormat)
              : ""}
          </Descriptions.Item>

          <Descriptions.Item label="Project Description">
            {ReactHtmlParser(project.description)}
          </Descriptions.Item>
          <Descriptions.Item label="Project Scope">
            {ReactHtmlParser(project.scope)}
          </Descriptions.Item>
          <Descriptions.Item label="Project Memo">
            {ReactHtmlParser(project.memo)}
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentProject: state.projectReducer.currentProject,
});

const mapDispatchToProps = {
  changeProject,
  deleteProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);
