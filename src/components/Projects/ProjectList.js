import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Button, Avatar ,Pagination} from "antd";

import {
  changeProject,
  fetchProjects,
  deleteProject,
} from "../../store/actions/projectActions";
import {
  fetchWorkitems
} from "../../store/actions/workitemActions";
import { Link } from "react-router-dom";

class ProjectList extends Component {
  componentDidMount() {
    this.props.fetchProjects(0,10);
  }
  selectProject = (project) => {
    this.props.changeProject(project);
    this.props.fetchWorkitems(project);
  };
  handleChange = value => {
    if(value)
      this.props.fetchProjects(value-1,10);
  };
  render() {
   const projects= this.props.projects;
    return (
      
      <div className="site-layout-content">
        <div>
          <Button type="link" href={`/projects-new`}>
            Add Project
          </Button>
        </div>
        <List
          itemLayout="horizontal"
          dataSource={projects.content}
          renderItem={(item) => (
            <List.Item>
              <List.Item.Meta
                avatar={
                  <Avatar src="https://www.flaticon.com/svg/static/icons/svg/2579/2579987.svg" />
                }
                title={item.id}
                description={
                  <>
                    {item.name}
                    <Link
                      onClick={() => this.selectProject(item)}
                      to={{
                        pathname: `projects-detail/${item.id}`,
                        state: { item },
                      }}
                    >
                      DETAIL
                    </Link>
                    <Link
                      onClick={() => this.selectProject(item)}
                      to={{
                        pathname: `workitems-list`,
                        state: { item },
                      }}
                    >
                      Workitem List
                    </Link>
                    <Link
                      onClick={() => this.selectProject(item)}
                      to={{
                        pathname: `workitems-new`,
                        state: { item },
                      }}
                    >
                      Add Workitem 
                    </Link>
                  </>
                }
              />
            </List.Item>
          )}
        />
        <Pagination defaultCurrent={1} total={projects.totalElements} onChange={this.handleChange}/>
      </div>
     /* <div>
      {JSON.stringify(projects)}
      </div>*/
    );
  }
}

const mapStateToProps = (state) => ({
  projects: state.projectReducer.projects,
  currentProject: state.projectReducer.currentProject,
});

const mapDispatchToProps = {
  changeProject,
  fetchProjects,
  deleteProject,
  fetchWorkitems
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);
