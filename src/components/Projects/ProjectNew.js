import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import ProjectForm from "./ProjectForm";
import { newProject, saveProject } from "../../store/actions/projectActions";

import { fetchUsers } from "../../store/actions/userActions";

class ProjectNew extends PureComponent {
  
  constructor(props) {
    super();
    this.state = {
      redirect: false,
      initialValues:{
        name: "",
        description: "",
        scope: "",
        memo: "",
        startDate: "",
        finishDate: "",
        selectOptionsStatus: [
          "Initation",
          "Planning",
          "Execution",
          "Monitor",
          "Closed",
        ],
      },
    };
  }

  componentDidMount = async () => {
    await this.props.newProject();
    await this.props.fetchUsers();
    const selectoptionsPM = this.props.usernames;
    this.setState({
      initialValues:{ ...this.state.initialValues, selectoptionsPM },
    });
  };
  handleSubmit = (formProps) => {
    const {
      name,
      description,
      scope,
      memo,
      projectMaster,
      startDate,
      finishDate,
      status,
    } = formProps;

    return this.props
      .saveProject(formProps)
      .then((response) => this.setState({ redirect: true }));
  };

  render() {

    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik
            initialValues={this.state.initialValues}
            enableReinitialize={true}
            onSubmit={this.handleSubmit}
          >
            {ProjectForm}
          </Formik>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  project: state.projectReducer.project,
  errors: state.projectReducer.errors,
  currentProject: state.projectReducer.currentProject,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map((user) => user["username"]),
});

const mapDispatchToProps = {
  newProject,
  saveProject,
  fetchUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectNew);
