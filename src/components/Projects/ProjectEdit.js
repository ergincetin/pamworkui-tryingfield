import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import ProjectForm from "./ProjectForm";

import { updateProject } from "../../store/actions/projectActions";
import { fetchUsers } from "../../store/actions/userActions";

class ProjectEdit extends Component {
  constructor(props) {
    super();
    this.state = {
      redirect: false,
    };
  }
  componentDidMount=async()=> {

   await this.props.fetchUsers();
  }


  submit = (project) => {
    return this.props
      .updateProject(project)
      .then((response) => this.setState({ redirect: true }))
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    const project = this.props.currentProject;
    if (!this.props.currentProject) {
      return <div>Loading...</div>;
    }
    const formValues = {
      id: project.id,
      name: project.name,
      description: project.description,
      scope: project.scope,
      memo: project.memo,
      startDate: project.startDate,
      finishDate: project.finishDate,
      status: project.status,
      projectMaster: project.projectMaster,
      selectoptionsPM: this.props.usernames,
      selectOptionsStatus: [
        "Initation",
        "Planning",
        "Execution",
        "Monitor",
        "Closed",
      ],
    };
    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik initialValues={formValues} onSubmit={this.submit}>
            {ProjectForm}
          </Formik>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentProject: state.projectReducer.currentProject,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map(user => user["username"])
});

const mapDispatchToProps = {
  updateProject,
  fetchUsers
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectEdit);
