import React, {useState} from "react";
import {Modal, Button, Card} from "antd"
// import {closeModal} from "../Modals/ModalActions"

const App = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);
  
    const showModal = () => {
      setIsModalVisible(true);
    };
  
    const handleOk = () => {
      setIsModalVisible(false);
    };
  
    const handleCancel = () => {
      setIsModalVisible(false);
    };
  
    const showContent = () => {
      return <p>Button content</p>;
    };
  
    return (
      <>
        <Button type="primary" onClick={showModal}>
          Open LogModal4
        </Button>
        <Modal style={{backgroundColor: "red", color: "darkblue"}}
          title="Basic Modal"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <Button onClick={()=>showContent}>Button sontent</Button>
          <p>Empty contents</p>
          <p>Some contents</p>
        </Modal>
      </>
    );
  };

  export default App;