import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Card, Button } from "antd";

import {
  changeProject,
  deleteProject,
} from "../../store/actions/projectActions";

import { Link } from "react-router-dom";
const style = { padding: "8px" };
class ProjectDelete extends Component {
  state = {
    redirect: false,
  };
  removeProject = (id) => {
    this.props
      .deleteProject(id)
      .then((response) => this.setState({ redirect: true }));
  };
  render() {
    const project = this.props.currentProject;
    if (!this.props.currentProject) {
      return <div>Loading...</div>;
    }

    return (
      <div className="site-layout-content">
        <>
          {this.state.redirect ? (
            <Redirect to="/projects-list" />
          ) : (
            <Card size="small" title="Project Delete" style={{ width: 300 }}>
              <p>{project.id} . kaydını silmek istiyor musunuz?</p>
              <Link
                as="Button"
                to={{
                  pathname: `/projects-details/${project.id}`,
                }}
              >
                Hayır
              </Link>
              <Button
                type="danger"
                onClick={() => this.removeProject(project.id)}
              >
                Evet
              </Button>
            </Card>
          )}
        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentProject: state.projectReducer.currentProject,
});

const mapDispatchToProps = {
  changeProject,
  deleteProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDelete);
