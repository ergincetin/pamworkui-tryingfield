import React from "react";
import {  Row, Col} from "antd";
import { Form, Field } from "formik";
import { AntDatePicker, AntInput, AntSelect } from "../../libs/CreateAndFields";
import { isRequired } from "../../libs/ValidateFields";
import { dateFormat } from "../../libs/FieldFormats";
import moment from "moment";


import ReactQuillFormik from "../../libs/ReactQuillFormik";

const ProjectForm = ({ handleSubmit, values, submitCount }) => {
  return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <Row>
        <Col span={24}>
          <Field
            component={AntInput}
            name="name"
            type="text"
            label="Name"
            validate={isRequired}
            submitCount={submitCount}
            hasFeedback
          />
        </Col>
      </Row>
     
      <Row>
      <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="status"
            defaultValue={values.status}
            selectOptions={values.selectOptionsStatus}
            label="Status"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={6}>
          <Field
            showSearch
            component={AntSelect}
            name="projectMaster"
            defaultValue={values.projectMaster}
            selectOptions={values.selectoptionsPM}
            label="Project Master"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={6}>
          <Field
            component={AntDatePicker}
            name="startDate"
            label="Start Date"
            value={values.startDate ? moment(values.startDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
        <Col span={6}>
          <Field
            component={AntDatePicker}
            name="finishDate"
            label="Finish Date"
            value={values.finishDate ? moment(values.finishDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
       
      </Row>
      <Row>
       <p>Description</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="description"
            type="text"
            label="Description"
            submitCount={submitCount}
          >
              {({ field }) => ReactQuillFormik(field)}
            </Field>
        </Col>
      </Row>
      <Row>
       <p>Scope</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="scope"
            type="text"
            label="Scope"
            submitCount={submitCount}>
                 {({ field }) =>  ReactQuillFormik(field)}
              </Field>
        </Col>
      </Row>
      <Row>
       <p>Memo</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="memo"
            type="text"
            label="Memo"
            submitCount={submitCount}>
               {({ field }) => ReactQuillFormik(field)}
            </Field>
        </Col>
      </Row>
      
      <Row>
        <div className="submit-container">
          <button className="ant-btn ant-btn-primary" type="submit">
            Submit
          </button>
        </div>
      </Row>
    </Form>
  );
};

export default ProjectForm;
