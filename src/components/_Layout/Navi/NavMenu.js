import React, { Component } from "react";
import { Layout, Menu } from "antd";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const { Header } = Layout;
const MenuItem = Menu.Item;

class NavMenu extends Component {
  conditionalRender() {
    if (this.props.security.validToken) {
      return (
        <React.Fragment>
          <Menu theme="dark"  mode="horizontal" defaultSelectedKeys={["2"]}>
            <MenuItem style={{height: "64px"}}>
              <Link to="/projects-list" style={{ fontSize: "22px", color: "white"}}>Home</Link>
            </MenuItem>

            <MenuItem  key="2" style={{height: "64px"}} >
              <Link  to="/projects-list" style={{ fontSize: "22px", color: "white"}}>Projects</Link>
            </MenuItem>

            <MenuItem style={{height: "64px"}}>
              <Link  to="/notes-list" style={{ fontSize: "22px", color: "white"}}>
                Notes</Link>
            </MenuItem>

            <MenuItem className="whitecolor" style={{height: "64px"}} >
              <Link  to="/signout" style={{ fontSize: "22px", color: "white"}}>Sign out</Link>
            </MenuItem>
 
            
          </Menu>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[""]}>
            <Menu.Item key="1" style={{ height: "64px" }}>
              <Link to="/" style={{ fontSize: "32px", color: "white" }}
              title="Home Page">
                PAMERA
              </Link>
            </Menu.Item>
            <Menu.Item key="2" style={{ height: "64px" }}>
              <Link to="/signin" style={{ fontSize: "22px", color: "white" }}>
                Sign in
              </Link>
            </Menu.Item>
            <Menu.Item key="3" style={{ height: "64px" }}>
              <Link to="/signup" style={{ fontSize: "22px", color: "white" }}>
                Sign up
              </Link>
            </Menu.Item>
          </Menu>
        </React.Fragment>
      );
    }
  }
  render() {
    return (
      <Layout>
        <Header style={{height: "64px"}}>{this.conditionalRender()}</Header>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => ({
  security: state.security,
  errot: state.error,
});

export default connect(mapStateToProps)(NavMenu);
