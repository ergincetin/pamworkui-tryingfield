import React from "react"
import {Input} from "antd"
import {SearchOutlined} from "@ant-design/icons"

const suffix = (
    
    <SearchOutlined
      style={{
        fontSize: 16,
        color: '#1890ff',
      }}
    />
  );
const SearchBar = () => {
    return (
        <div> 
    <Input allowClear placeholder="Search for notes" 
    style={{width: 200}} suffix={suffix}/>
    </div>
    )
    
}

export default SearchBar;