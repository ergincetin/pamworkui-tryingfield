import React, { Component } from "react";
import { connect } from "react-redux";
import {Layout } from "antd"



const { Content } = Layout;


class Home extends Component {
  componentDidMount() {
    if (this.props.security.validToken) {
      this.props.history.push("/projects");
    }
  }
  render() {
    return (
      <Layout>
        <Content align="center"
        style={{ backgroundImage: "(../)"}}>
          <br></br>
        <h1 style={{fontSize:"58px", color:"darkblue"}}>Welcome to Pamera - Task Manager</h1>
        </Content>
      </Layout>
      
    );
  }
}

const mapStateToProps = (state) => ({
  security: state.security,
});

export default connect(mapStateToProps)(Home);
