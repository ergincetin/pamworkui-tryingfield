import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchTasks, deleteTask ,changeTask} from "../../store/actions/taskActions";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import { Button,Row, Col,Collapse ,List} from "antd";
import ReactHtmlParser from "react-html-parser";

const { Panel } = Collapse;

class TaskList extends Component {
 
  selectTask = (task) => {
    this.props.changeTask(task);
  };
  removeTask = (id) => {
    this.props.deleteTask(id);
  };
  render() {
    const tasks = this.props.tasks;
    return (
      <div>
               <br></br>
        <br></br>
        <Row>
          <Col sm={7}>
            <h3>Task List</h3>
          </Col>
          <Col></Col>
          <Col sm={2}>
           {/*  <Button as={Link} to="/tasks-new" variant="outline-primary">
              New Workitem
            </Button> */}
          </Col>
        </Row>
        <Collapse accordion>
          {tasks.map((task) => (
            <Panel header={task.name} key={task.id}>
              <Row>
                <Col>
                <Link
                      onClick={() => this.selectTask(task)}
                      to={{
                        pathname: `tasks-edit/${task.id}`,
                        state: { task },
                      }}
                    >
                      Edit Edit
                    </Link>-
                    <Link
                      onClick={() => this.selectTask(task)}
                      to={{
                        pathname: `tasks-detail/${task.id}`,
                        state: { task },
                      }}
                    >
                      Task Detail
                    </Link>-
                    <Link
                      onClick={() => this.selectTask(task)}
                      to={{
                        pathname: `tasks-delete/${task.id}`,
                        state: { task },
                      }}
                    >
                      Delete Task
                    </Link>
                </Col>
              </Row>
              <Row>
                <Col>Hours</Col>
                <Col>Due Date</Col>
                <Col>Expected Date</Col>
                <Col>Actual Date</Col>
                <Col>Assign To</Col>
                <Col>State</Col>
                <Col>Category </Col>
                <Col>Type</Col>
              </Row>
              <Row>
                <Col>{task.hoursExpected}</Col>
                <Col>
                  {task.dueDate === undefined
                    ? new Date(task.dueDate).toLocaleDateString()
                    : task.dueDate}
                </Col>
                <Col>
                  {task.expectedDate === undefined
                    ? new Date(task.expectedDate).toLocaleDateString()
                    : task.expectedDate}
                </Col>
                <Col>
                  {task.actualDate === undefined
                    ? new Date(task.actualDate).toLocaleDateString()
                    : task.actualDate}
                </Col>
                <Col>{task.assignTo}</Col>
                <Col>{task.status}</Col>
                <Col>{task.category}</Col>
                <Col>{task.type}</Col>
              </Row>
              <p>{ReactHtmlParser(task.description)}</p>
            </Panel>
          ))}
        </Collapse>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  tasks: state.taskReducer.tasks,
  currentWorkitem: state.workitemReducer.currentWorkitem,
});

const mapDispatchToProps = { fetchTasks, deleteTask,changeTask };

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
