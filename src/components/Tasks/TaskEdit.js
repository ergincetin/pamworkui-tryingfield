import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import TaskForm from "./TaskForm";

import { updateTask } from "../../store/actions/taskActions";
import { fetchUsers } from "../../store/actions/userActions";

class TaskEdit extends Component {
  constructor(props) {
    super();
    this.state = {
      redirect: false,
    };
  }
  componentDidMount = async () => {
    await this.props.fetchUsers();
  };

  submit = (task) => {
    return this.props
      .updateTask(task)
      .then((response) => this.setState({ redirect: true }))
      .catch((error) => {
        console.error(error);
      });
  };

  render() {
    const task = this.props.currentTask;
    if (!this.props.currentTask) {
      return <div>Loading...</div>;
    }
    const formValues = {
      id: task.id,
      name: task.name,
      description: task.description,
      hoursExpected: task.hoursExpected,
      hoursActual:task.hoursActual,
      dueDate: task.dueDate,
      expectedDate: task.expectedDate,
      actualDate: task.actualDate,
      assignTo: task.assignTo,
      type: task.type,
      category: task.category,
      status: task.status,
      workitemid: task.workitemid,
      selectoptionsAT: this.props.usernames,
      selectOptionsStatus: [
        "Cancelled",
        "Blocked",
        "ToDo",
        "InProgress",
        "ToVerify",
        "Done",
      ],
      selectOptionsCategory: [
        "Analysis",
        "Development",
        "Test",
        "Operation",
        "Document",
      ],
      selectOptionsType: ["Task"],
    };
    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik initialValues={formValues} enableReinitialize={true} onSubmit={this.submit}>
            {TaskForm}
          </Formik>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentTask: state.taskReducer.currentTask,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map((user) => user["username"]),
});

const mapDispatchToProps = {
  updateTask,
  fetchUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskEdit);
