import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Card, Button } from "antd";
import { changeTask, deleteTask } from "../../store/actions/taskActions";
import { Link } from "react-router-dom";

class TaskDelete extends PureComponent {
  state = {
    redirect: false,
  };
  removeTask = (id) => {
    this.props
      .deleteTask(id)
      .then((response) => this.setState({ redirect: true }));
  };

  render() {
    const task = this.props.currentTask;
    if (!this.props.currentTask) {
      return <div>Loading...</div>;
    }

    return (
      <div className="site-layout-content">
        <>
          {this.state.redirect ? (
            <Redirect to="/projects-list" />
          ) : (
            <Card size="small" title="Task Delete" style={{ width: 300 }}>
              <p>{task.id} . kaydını silmek istiyor musunuz?</p>
              <Link
                as="Button"
                to={{
                  pathname: `/tasks-details/${task.id}`,
                }}
              >
                No
              </Link>
              <Button type="danger" onClick={() => this.removeTask(task.id)}>
                Yes
              </Button>
            </Card>
          )}
        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentTask: state.taskReducer.currentTask,
});

const mapDispatchToProps = {
  changeTask,
  deleteTask,
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskDelete);
