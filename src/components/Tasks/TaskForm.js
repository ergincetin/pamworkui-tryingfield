import React from "react";
import { Row, Col } from "antd";
import { Form, Field } from "formik";
import { AntDatePicker, AntInput, AntSelect } from "../../libs/CreateAndFields";
import { isRequired } from "../../libs/ValidateFields";
import { dateFormat } from "../../libs/FieldFormats";
import moment from "moment";

import ReactQuillFormik from "../../libs/ReactQuillFormik";

const TaskForm = ({ handleSubmit, values, submitCount }) => {
  return (
    <Form className="form-container" onSubmit={handleSubmit}>
      <Row>
        <Col span={24}>
          <Field
            component={AntInput}
            name="name"
            type="text"
            label="Name"
            validate={isRequired}
            submitCount={submitCount}
            hasFeedback
          />
        </Col>
      </Row>
      <Row>
       
        <Col span={8}>
          <Field
            showSearch
            component={AntSelect}
            name="status"
            defaultValue={values.status}
            selectOptions={values.selectOptionsStatus}
            label="Status"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={8}>
          <Field
            showSearch
            component={AntSelect}
            name="category"
            defaultValue={values.category}
            selectOptions={values.selectOptionsCategory}
            label="Category"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={8}>
          <Field
            showSearch
            component={AntSelect}
            name="type"
            defaultValue={values.type}
            selectOptions={values.selectOptionsType}
            label="Type"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
      </Row>
      <Row>
      <Col span={4}>
          <Field
            showSearch
            component={AntSelect}
            name="assignTo"
            defaultValue={values.assignTo}
            selectOptions={values.selectoptionsAT}
            label="Assign To"
            tokenSeparators={[","]}
            submitCount={submitCount}
            allowClear
          />
        </Col>
        <Col span={4}>
          <Field
            component={AntInput}
            name="hoursExpected"
            type="text"
            label="Hours Expected"
            submitCount={submitCount}
          />
        </Col>
        <Col span={4}>
          <Field
            component={AntInput}
            name="hoursActual"
            type="text"
            label="Hours Actual"
            submitCount={submitCount}
          />
        </Col>
        <Col span={4}>
          <Field
            component={AntDatePicker}
            name="dueDate"
            label="Due Date"
            value={values.dueDate ? moment(values.dueDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
        <Col span={4}>
          <Field
            component={AntDatePicker}
            name="expectedDate"
            label="Expected Date"
            value={values.expectedDate ? moment(values.expectedDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
        <Col span={4}>
          <Field
            component={AntDatePicker}
            name="actualDate"
            label="Actual Date"
            value={values.actualDate ? moment(values.actualDate) : NaN}
            format={dateFormat}
            submitCount={submitCount}
          />
        </Col>
      </Row>
      <Row>
        <p>Description</p>
      </Row>
      <Row>
        <Col span={24}>
          <Field
            name="description"
            type="text"
            label="Description"
            submitCount={submitCount}
          >
            {({ field }) => ReactQuillFormik(field)}
          </Field>
        </Col>
      </Row>
    
      <Row>
        <div className="submit-container">
          <button className="ant-btn ant-btn-primary" type="submit">
            Submit
          </button>
        </div>
      </Row>
    </Form>
  );
};

export default TaskForm;
