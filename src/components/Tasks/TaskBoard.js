import { PureComponent } from "react";
import { Card, Collapse } from "antd";

const { Panel } = Collapse;

class TaskBoard extends PureComponent {
  render() {
    const tasks = this.props.tasks;

    let cancelledTasks = [];
    let blockedTasks = [];
    let inBacklogTasks = [];
    let toDoTasks = [];
    let wipTasks = [];
    let doneTasks = [];
    let noStateTasks = [];

    for (let i = 0; i < tasks.length; i++) {
      if (
        tasks[i].state === "" ||
        tasks[i].state === undefined ||
        tasks[i].state === null
      ) {
        noStateTasks.push(tasks[i]);
      }
      if (tasks[i].state === "Cancelled") {
        cancelledTasks.push(tasks[i]);
      }

      if (tasks[i].state === "Blocked") {
        blockedTasks.push(tasks[i]);
      }

      if (tasks[i].state === "InBackLog") {
        inBacklogTasks.push(tasks[i]);
      }

      if (tasks[i].state === "ToDo") {
        toDoTasks.push(tasks[i]);
      }

      if (tasks[i].state === "WIP") {
        wipTasks.push(tasks[i]);
      }

      if (tasks[i].state === "Done") {
        doneTasks.push(tasks[i]);
      }
    }

    return (
      <Collapse>
        <Card style={{ borderColor: "red" }} title="Board">
          <Panel collapsible header="Progress">
            <Card title="In Backlog Tasks">
              {inBacklogTasks.map((inbacklogitem) => (
                <p key={inbacklogitem.id}>{inbacklogitem.name}</p>
              ))}
            </Card>
            <Card title="To Do Tasks">
              {toDoTasks.map((todoitem) => (
                <p key={todoitem.id}>{todoitem.name}</p>
              ))}
            </Card>
            <Card title="WIP Tasks">
              {wipTasks.map((wipitem) => (
                <p key={wipitem.id}>{wipitem.name}</p>
              ))}
            </Card>
            <Card title="Done Tasks">
              {doneTasks.map((donetem) => (
                <p key={donetem.id}>{donetem.name}</p>
              ))}
            </Card>
          </Panel>
          <Panel header="Risk">
            <Card title="Blocked Tasks">
              {blockedTasks.map((blockeditem) => (
                <p key={blockeditem.id}>{blockeditem.name}</p>
              ))}
            </Card>

            <Card title="Cancelled Tasks">
              {cancelledTasks.map((cancelleditem) => (
                <p key={cancelleditem.id}>{cancelleditem.name}</p>
              ))}
            </Card>
            <Card title="No-state Tasks">
              {noStateTasks.map((nostateitem) => (
                <p key={nostateitem.id}>{nostateitem.name}</p>
              ))}
            </Card>
          </Panel>
        </Card>
      </Collapse>
    );
  }
}
export default TaskBoard;
