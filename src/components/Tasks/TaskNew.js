import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { Formik } from "formik";
import TaskForm from "./TaskForm";
import { newTask, saveTask } from "../../store/actions/taskActions";

import { fetchUsers } from "../../store/actions/userActions";

class TaskNew extends PureComponent {
  
  constructor(props) {
    super();
    this.state = {
      redirect: false,
      initialValues:{
        name: "",
        description: "",
        hoursExpected: "",
        hoursActual:"",
        dueDate: "",
        expectedDate: "",
        actualDate: "",
        assignTo:"",
        status:"",
        type:"",
        category:"",
        workitemid:"",
        selectOptionsStatus: [
          "Cancelled",
          "Blocked",
          "ToDo",
          "InProgress",
          "ToVerify",
          "Done"
        ],
        selectOptionsCategory: [
            "Analysis",
            "Development",
            "Test",
            "Operation",
            "Document"
          ],
          selectOptionsType: [
            "Task"
          ],
      },
    };
  }
// :   Cancelled(-1),Blocked(0),InBackLog(1),WIP(2),ReadyToTest(3),InTest(4),Deployed(5),Released(6);
// Category: Analysis(1),Development(2),Test(3),Operation(4),Document(5);
// Status:     Cancelled(-1),Blocked(0),ToDo(1),InProgress(2),ToVerify(3),Done(4);
  componentDidMount = async () => {
    await this.props.newTask();
    await this.props.fetchUsers();
    const selectoptionsAT = this.props.usernames;
    const currentWorkitem= this.props.currentWorkitem;
    this.setState({
      initialValues:{ ...this.state.initialValues, selectoptionsAT,workitemid:currentWorkitem.id},
    });
  };
  handleSubmit = (formProps) => {
    const {
        name,
        description,
        hoursExpected,
        hoursActual,
        dueDate,
        expectedDate,
        actualDate,
        assignTo,
        status,
        type,
        category,
        workitemid
    } = formProps;

    return this.props
      .saveTask(formProps)
      .then((response) => this.setState({ redirect: true }));
  };

  render() {

    return (
      <div className="site-layout-content">
        {this.state.redirect ? (
          <Redirect to="/projects-list" />
        ) : (
          <Formik
            initialValues={this.state.initialValues}
            enableReinitialize={true}
            onSubmit={this.handleSubmit}
          >
            {TaskForm}
          </Formik>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  task: state.taskReducer.task,
  errors: state.taskReducer.errors,
  currentWorkitem: state.workitemReducer.currentWorkitem,
  users: state.userReducer.users,
  usernames: state.userReducer.users.map((user) => user["username"]),
});

const mapDispatchToProps = {
  newTask,
  saveTask,
  fetchUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskNew);
