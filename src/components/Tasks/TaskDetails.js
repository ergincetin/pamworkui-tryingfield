import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Descriptions, Row, Col } from "antd";
import {
  changeTask,
  deleteTask,
} from "../../store/actions/taskActions";
import moment from "moment";
import { dateFormat, timeFormat } from "../../libs/FieldFormats";
import { Link } from "react-router-dom";
import ReactHtmlParser from "react-html-parser";

const style = { padding: "8px" };

class TaskDetails extends PureComponent {
    selectTask = (task) => {
        this.props.changeTask(task);
    }

    render() {
        const task = this.props.currentTask;
        if(!this.props.currentTask){
            return <div>Loading...</div>;
        }
        return (
            <div className="site-layout-content">
               <Descriptions
          bordered
          title="Task Detail"
          extra={
            <div>
              <Row gutter={16}>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectTask(task)}
                      to={{
                        pathname: `/tasks-edit/${task.id}`,
                        state: { task },
                      }}
                    >
                      Edit
                    </Link>
                  </div>
                </Col>
                <Col className="gutter-row" span={8}>
                  <div style={style}>
                    <Link
                      onClick={() => this.selectTask(task)}
                      to={{
                        pathname: `/tasks-delete/${task.id}`,
                        state: { task },
                      }}
                    >
                      Delete
                    </Link>
                  </div>
                </Col>
              </Row>
            </div>
          }
        >
          <Descriptions.Item label="Id">{task.id}</Descriptions.Item>
          <Descriptions.Item label="Name">{task.name}</Descriptions.Item>
          <Descriptions.Item label="Task Expected hours">
            {task.expectedHour}
          </Descriptions.Item>
          <Descriptions.Item label="Task Expected hours">
            {task.actualHour}
          </Descriptions.Item>
          <Descriptions.Item label="Task Status">
            {task.status}
          </Descriptions.Item>

          <Descriptions.Item label="Task Assign To">
            {ReactHtmlParser(task.assignTo)}
          </Descriptions.Item>

          <Descriptions.Item label="Task Due Date">
            {task.startDate
              ? moment(task.dueDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
          <Descriptions.Item label="Task Expected Date">
            {task.expectedDate
              ? moment(task.expectedDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
            
          <Descriptions.Item label="Actual Date">
            {task.actualDate
              ? moment(task.actualDate).format(dateFormat)
              : ""}
          </Descriptions.Item>
          <Descriptions.Item label="Task Type">
            {ReactHtmlParser(task.type)}
          </Descriptions.Item>
          <Descriptions.Item label="Task Category">
            {ReactHtmlParser(task.category)}
          </Descriptions.Item>
        </Descriptions>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    currentTask: state.taskReducer.currentTask,
});

const mapDispatchToProps = {
    changeTask,
    deleteTask,
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetails);