import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import { register } from "../../store/actions/securityActions";
import { Card, Col, Row } from "antd";
import { Link } from "react-router-dom";
import RegisterForm from "./RegisterForm";

const initialValues = {
  firstname: "",
  lastname: "",
  username: "",
  email: "",
  password: "",
};

const { Meta } = Card;

class Signup extends PureComponent {
  state = {
    redirect: false,
  };
  handleSubmit = (formProps) => {
    const { firstname, lastname, username, email, password } = formProps;
    this.props.register(formProps, () => {
      this.props.history.push("/");
    });
    this.setState({ redirect: true });
  };

  render = () => (
    <Row>
      <Col span={8}></Col>
      <Col span={8}>
        <Card title="Sign Up" style={{ width: 400 }}>
          <Formik initialValues={initialValues} onSubmit={this.handleSubmit}>{RegisterForm}</Formik>
        </Card>
      </Col>

      <Col span={8}></Col>
    </Row>
  );
}

const mapStateToProps = (state) => ({
  security: state.security,
  errorMessage: state.errors,
});

const mapDispatchToProps = { register };

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
