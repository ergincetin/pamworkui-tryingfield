import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Formik } from "formik";
import LoginForm from './LoginForm';
import {login} from '../../store/actions/securityActions';
import {Card, Row, Col } from 'antd';

const initialValues = {
  username: "",
  password: ""
};

class Signin extends PureComponent {
  componentDidMount() {
    if (this.props.security.validToken) {
      this.props.history.push("/projects-list");
      
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.security.validToken) {
      this.props.history.push("/projects-list");
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  handleSubmit = formProps => {
    const {username,password } = formProps;

    this.props.login(formProps, () => {
      this.props.history.push('/projects');
    }
    );

  };

  render = () => (
    <Row>

      <Col span={8}></Col>
      <Col span={8}>
    
    <Card title="Login"  style={{ width: 400 }}>
        <Formik initialValues={initialValues} onSubmit={this.handleSubmit}>{LoginForm}</Formik>
    <br></br>
    </Card>
    </Col>
    <Col span={8}/>
    </Row>
  
  );
}

const mapStateToProps = (state) => ({
  security:state.security,
  errorMessage:state.errors
 
})

const mapDispatchToProps = {login}


export default connect(mapStateToProps, mapDispatchToProps)(Signin);
