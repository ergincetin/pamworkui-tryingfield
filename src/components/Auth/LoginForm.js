import React from "react";
import { Form, Field } from "formik";
import { AntInput,AntPassword } from "../../libs/CreateAndFields";
import {
  isRequired
} from "../../libs/ValidateFields";
import {Link} from "react-router-dom"
import {Row, Col} from "antd"

const LoginForm = ({ handleSubmit, reset, values, submitCount }) => (
  <Form className="form-container" onSubmit={handleSubmit}>
    <label>User name: </label>
    <Field
      component={AntInput}
      name="username"
      type="text"
      validate={isRequired}
      submitCount={submitCount}
      hasFeedback
    />
    <label>Password: </label>
    <Field
      component={AntPassword}
      name="password"
      type="password"
      validate={isRequired}
      submitCount={submitCount}
      hasFeedback
    />
    <div className="submit-container">
      <Row>
          <Col><button  className="ant-btn ant-btn-primary" type="submit">
        Sign in 
      </button></Col>
      </Row>
      
      <br></br><br></br>
      <label>Do not have an account? </label>
      <br></br>
      <br></br>
      <Link style={{color: "black", fontFamily: "bold"}} to="/signup">
     {'=>'} Go to Sign up
        </Link>
    </div>
  </Form>
);

export default LoginForm;